package main

type ErrorResponse struct {
	Error string `json:"error"`
}

type Version struct {
	Version   string `json:"version"`
	Commit    string `json:"commit"`
	BuildTime string `json:"buildTime"`
}
