package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"
)

var (
	version, commit, buildTime string
	versions                   Versions
	censored                   = make(map[string]bool)
	censoredRWM                sync.RWMutex
)

func main() {
	http.HandleFunc("/api/v1/version", endpoint1)
	http.HandleFunc("/api/v1/author", endpoint2)
	http.HandleFunc("/api/v1/book", endpoint3)
	http.HandleFunc("/api/v1/consors", endpoint4)

	// log.Fatal(http.ListenAndServe(":8000", nil))

	/*mux := http.NewServeMux()
	/*mux.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		fmt.Fprintf(w, "Hello!")
	})

	mux.HandleFunc("/api/v1/version", endpoint1)
	mux.HandleFunc("/api/v1/author", endpoint2)
	mux.HandleFunc("/api/v1/book", endpoint3)
	mux.HandleFunc("/api/v1/consors", endpoint4)
	*/
	httpServer := &http.Server{
		Addr: ":8000",
	}

	// Run server
	go func() {
		if err := httpServer.ListenAndServe(); err != http.ErrServerClosed {
			log.Fatalf("HTTP server ListenAndServe: %v", err)
		}
	}()

	signalChan := make(chan os.Signal, 1)

	signal.Notify(
		signalChan,
		syscall.SIGHUP,  // kill -SIGHUP XXXX
		syscall.SIGINT,  // kill -SIGINT XXXX or Ctrl+c
		syscall.SIGQUIT, // kill -SIGQUIT XXXX
	)

	<-signalChan
	log.Print("os.Interrupt - shutting down...\n")

	go func() {
		<-signalChan
		log.Fatal("os.Kill - terminating...\n")
	}()

	gracefullCtx, cancelShutdown := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancelShutdown()

	if err := httpServer.Shutdown(gracefullCtx); err != nil {
		log.Printf("shutdown error: %v\n", err)
		os.Exit(1)
	} else {
		log.Printf("gracefully stopped\n")
	}

	os.Exit(0)
}

func endpoint1(w http.ResponseWriter, req *http.Request) {
	versions = Versions{version, commit, buildTime}
	if req.Method != http.MethodGet {
		sendError(w, http.StatusBadRequest, errors.New("unsupported method. GET method is supported only"))
		return
	} else {
		resp, err := json.Marshal(versions)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		w.Write(resp)
		return
	}

}

func endpoint2(w http.ResponseWriter, req *http.Request) {
	var key Author
	var name Authorname

	if req.Method != http.MethodGet {
		sendError(w, http.StatusBadRequest, errors.New("unsupported method. GET method is supported only"))
		return
	}

	isbn := req.URL.Query().Get("book")
	if isbn == "" {
		sendError(w, http.StatusBadRequest, errors.New("missing URL param book which has to contain isbn of book"))
		return
	}

	url := "https://openlibrary.org/isbn/" + isbn + ".json"

	body, err := sendRequest(url)
	if err != nil {
		sendError(w, http.StatusInternalServerError, err)
		return
	}

	json.Unmarshal(body, &key)

	// iterate through author keys in bookInfo
	// for every key send a new request to API and retrieve Author's name
	result := make([]AuthorResponse, len(key.Authors))
	for index := range key.Authors {

		url := "https://openlibrary.org" + key.Authors[index].Key + ".json"
		body, err = sendRequest(url)
		if err != nil {
			sendError(w, http.StatusInternalServerError, err)
			return
		}

		err := json.Unmarshal(body, &name)
		if err != nil {
			fmt.Println("error:", err)
			sendError(w, http.StatusInternalServerError, errors.New("cannot create error"))
		}

		key.Authors[index].Key = strings.ReplaceAll(key.Authors[index].Key, "/authors/", "")

		result[index].AuthorName = name.Name
		result[index].Key = key.Authors[index].Key

	}
	resp, err := json.Marshal(result)
	if err != nil {
		log.Println(err)
		sendError(w, http.StatusInternalServerError, errors.New("cannot create error"))
	}
	w.Write(resp)
}

func endpoint3(w http.ResponseWriter, req *http.Request) {

	time.Sleep(10 * time.Second)

	var sc http.Response
	authorBooks := Entries{}

	if req.Method != http.MethodGet {
		sendError(w, http.StatusBadRequest, errors.New("unsupported method. GET method is supported only"))
		return
	}

	author := req.URL.Query().Get("book")
	if author == "" {
		sendError(w, http.StatusBadRequest, errors.New("missing URL param book which has to contain isbn of book"))
		return
	}

	if isCensored(author) {
		sendError(w, http.StatusBadRequest, errors.New("author is censored"))
		return
	}

	url := "https://openlibrary.org/authors/" + author + "/works.json?limit=2"

	body, err := sendRequest(url)
	if err != nil {
		sendError(w, http.StatusInternalServerError, err)
		return
	}

	err = json.Unmarshal(body, &authorBooks)
	if err != nil {
		fmt.Println("error:", err)
		sendError(w, http.StatusInternalServerError, errors.New("cannot create error"))
	}

	respB := make([]RespBooks, len(authorBooks.Entries))

	for index := range authorBooks.Entries {

		respB[index].Name = authorBooks.Entries[index].Title
		respB[index].Key = authorBooks.Entries[index].Key
		respB[index].Revision = authorBooks.Entries[index].Revision
		respB[index].PublishDate = authorBooks.Entries[index].Created.Value
	}

	resp, err := json.Marshal(respB)
	if err != nil {
		log.Println(err)
		sendError(w, http.StatusInternalServerError, errors.New("cannot create error"))
	}
	w.Write(resp)
	logging(req, sc)
}

func endpoint4(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPost {
		sendError(w, http.StatusBadRequest, errors.New("unsupported method. POST method is supported only"))
		return
	}

	censors := make([]string, 0)
	err := json.NewDecoder(req.Body).Decode(&censors)
	if err != nil {
		log.Print("error", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	addCensored(censors)
}

func addCensored(censors []string) {
	censoredRWM.Lock()

	for _, value := range censors {
		censored[value] = true
	}

	censoredRWM.Unlock()
}

func isCensored(author string) bool {
	censoredRWM.Lock()
	defer censoredRWM.Unlock()
	return censored[author]
}

func sendRequest(url string) ([]byte, error) {
	res, err := http.Get(url)
	if err != nil {
		log.Println(err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println(err)
	}

	return body, err
}

func sendError(w http.ResponseWriter, httpCode int, err error) {
	buf, locErr := json.Marshal(ErrorResponse{Error: err.Error()})
	if locErr != nil {
		log.Println("origin error", err)
		log.Println("json marshall error", locErr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(httpCode)
	w.Write(buf)
}

func logging(req *http.Request, sc http.Response) {
	// fmt.Println("Client IP: \n", req.RemoteAddr, "HTTP Method: ", req.Method, "URL: ", req.URL, "Time: ", time.Now(), "Status Code: ", sc.StatusCode)
	fmt.Print("Client IP: ", req.RemoteAddr, "\n", "HTTP Method: ", req.Method, "\n", "URL: ", req.URL, "\n", "Time: ", time.Now(), "\n", "Status Code: ", sc.StatusCode, "\n")
}
