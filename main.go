package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

var (
	appVersion, commit, buildTime string
	version                       Version
)

func main() {

	version = Version{appVersion, commit, buildTime}
	versionHandler := func(w http.ResponseWriter, req *http.Request) {

		if req.Method != http.MethodGet {

			u, err := json.Marshal(ErrorResponse{"wrong request"})
			if err != nil {
				log.Println("Error", err)
				return
			}
			fmt.Println(string(u)) // {"full_name":"Bob"}

			w.WriteHeader(http.StatusBadRequest)
			w.Write(u)

			return
		}
		io.WriteString(w, "Version\n")

		u, err := json.Marshal(version)
		if err != nil {
			log.Println("Error", err)
		}
		w.Write(u)
	}

	http.HandleFunc("/api/v1/version", versionHandler)
	log.Fatal(http.ListenAndServe(":8000", nil))
}
